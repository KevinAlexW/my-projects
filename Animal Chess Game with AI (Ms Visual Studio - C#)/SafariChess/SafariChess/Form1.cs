﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SafariChess
{
    public partial class Form1 : Form
    {
        string giliran = "Player 1";
        int counter = -1;
        Button[,] arrbtn = new Button[9, 7];
        Label[,] arrlabel = new Label[9, 7];
        Point[] clickedloc = new Point[2];
        Board current_board;
        public Form1()
        {
            InitializeComponent();
            DoubleBuffered = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = giliran + "'s Turn";
            //buat dynamic comp buttons + labels
            /*
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    Button newbtn = new Button();
                    newbtn.Size = new Size(44, 44);
                    newbtn.Location = new Point((j+1)*44, (i+1)*44);
                    newbtn.Text = "";
                    arrbtn[i, j] = newbtn;
                    arrbtn[i, j].Click += Form1_Click;
                    this.Controls.Add(newbtn);

                    Label newlabel = new Label();
                    newlabel.Size = new Size(22, 22);
                    newlabel.Location = new Point((j + 1) * 44 + 11, (i + 1) * 44 + 11);
                    newlabel.BackColor = Color.White;
                    arrlabel[i, j] = newlabel;
                    this.Controls.Add(newlabel);
                    newlabel.BringToFront();
                }
            }
            */

            //buat board baru
            Board b = new Board();

            //set pawn pawn musuh
            //row 1
            b.map[0, 0].hewan = new Animal("Lio", 7, "Player 2", SafariChess.Properties.Resources.Dou_Shou_Qi_Lion_194528);
            b.map[0, 6].hewan = new Animal("Tig", 6, "Player 2", SafariChess.Properties.Resources.Dou_Shou_Qi_Tiger_194529);

            //row 2
            b.map[1, 1].hewan = new Animal("Dog", 3, "Player 2", SafariChess.Properties.Resources.Dou_Shou_Qi_Dog_194532);
            b.map[1, 5].hewan = new Animal("Cat", 2, "Player 2", SafariChess.Properties.Resources.Dou_Shou_Qi_Cat_194533);

            //row 3
            b.map[2, 0].hewan = new Animal("Mou", 1, "Player 2", SafariChess.Properties.Resources.Dou_Shou_Qi_Rat_194534);
            b.map[2, 2].hewan = new Animal("Jag", 5, "Player 2", SafariChess.Properties.Resources.Dou_Shou_Qi_Leopard_194530);
            b.map[2, 4].hewan = new Animal("Wol", 4, "Player 2", SafariChess.Properties.Resources.Dou_Shou_Qi_Wolf_194531);
            b.map[2, 6].hewan = new Animal("Ele", 8, "Player 2", SafariChess.Properties.Resources.Dou_Shou_Qi_Elephant_194527);

            //set petak air, trap, goal - dipindah di class #Mitchell

            //set pawn pawn player
            //row 6
            b.map[6, 0].hewan = new Animal("Ele", 8, "Player 1", SafariChess.Properties.Resources.Dou_Shou_Qi_Elephant_194527);
            b.map[6, 2].hewan = new Animal("Wol", 4, "Player 1", SafariChess.Properties.Resources.Dou_Shou_Qi_Wolf_194531);
            b.map[6, 4].hewan = new Animal("Jag", 5, "Player 1", SafariChess.Properties.Resources.Dou_Shou_Qi_Leopard_194530);
            b.map[6, 6].hewan = new Animal("Mou", 1, "Player 1", SafariChess.Properties.Resources.Dou_Shou_Qi_Rat_194534);

            //row 7
            b.map[7, 1].hewan = new Animal("Cat", 2, "Player 1", SafariChess.Properties.Resources.Dou_Shou_Qi_Cat_194533);
            b.map[7, 5].hewan = new Animal("Dog", 3, "Player 1", SafariChess.Properties.Resources.Dou_Shou_Qi_Dog_194532);

            //row 8
            b.map[8, 0].hewan = new Animal("Tig", 6, "Player 1", SafariChess.Properties.Resources.Dou_Shou_Qi_Tiger_194529);
            b.map[8, 6].hewan = new Animal("Lio", 7, "Player 1", SafariChess.Properties.Resources.Dou_Shou_Qi_Lion_194528);

            current_board = b;

            //untuk draw
            clickedloc[0] = new Point(-1, -1);
            clickedloc[1] = new Point(-1, -1);
        }

        /*
        private void Form1_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if(counter == -1)
            {
                if(current_board.map[(btn.Location.Y / 44) - 1, (btn.Location.X / 44) - 1].hewan != null)
                {
                    if (current_board.map[(btn.Location.Y / 44) - 1, (btn.Location.X / 44) - 1].hewan.belongsTo == giliran)
                    {
                        counter += 1;
                        clickedloc[counter] = new Point((btn.Location.X / 44) - 1, (btn.Location.Y / 44) - 1);
                    } 
                }
            }
            else
            {
                counter += 1;
                clickedloc[counter] = new Point((btn.Location.X / 44) - 1, (btn.Location.Y / 44) - 1);
            }
            if(counter == 1)
            {
                //sudah klik button 2x, tentukan action
                if(current_board.movePawn(clickedloc[0].X, clickedloc[0].Y, clickedloc[1].X, clickedloc[1].Y, giliran))
                {
                    //move yang dilakukan valid, ganti giliran
                    if(giliran == "Player")
                    {
                        giliran = "AI";
                        AIMove();
                    }
                    else
                    {
                        giliran = "Player";
                    }
                }
                else
                {
                    //move yang dilakukan tidak valid, masih giliran yg sama
                    if(giliran == "AI")
                    {
                        AIMove();
                    }
                }
                label1.Text = giliran + "'s Turn";
                counter = -1;
                clickedloc = new Point[2];
                setBoard();
            }
        }

        public void setBoard()
        {
            //set background image dr buttons
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    arrlabel[i, j].Text = "";
                    //tidak ada hewan yang menempati
                    if(current_board.map[i, j].isLand)
                    {
                        //daratan
                        arrbtn[i, j].BackColor = Color.Green;
                    }
                    else
                    {
                        //air
                        arrbtn[i, j].BackColor = Color.Blue;
                    }
                    if (current_board.map[i, j].isTrap)
                    {
                        arrlabel[i, j].Text = "x";
                    }
                    else if (current_board.map[i, j].isGoal)
                    {
                        arrlabel[i, j].Text = "*";
                    }
                    if (current_board.map[i, j].hewan != null)
                    {
                        //di tile ini ada hewan
                        arrlabel[i, j].Text = current_board.map[i, j].hewan.l;
                    }
                }
            }
        }
        */

        public void AIMove()
        {

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            bool left = false;
            bool right = false;
            bool up = false;
            bool down = false;

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    int x = (j + 1) * 44;
                    int y = (i + 1) * 44;
                    if (current_board.map[i, j].isLand)
                    {
                        if ((i + j) % 2 == 1)
                        {
                            e.Graphics.FillRectangle(Brushes.Green, x, y, 43, 43);
                        }
                        else
                        {
                            e.Graphics.FillRectangle(Brushes.GreenYellow, x, y, 43, 43);
                        }
                    }
                    else if (!current_board.map[i, j].isLand)
                    {
                        e.Graphics.FillRectangle(Brushes.LightBlue, x, y, 43, 43);
                    }

                    if (current_board.map[i, j].isTrap)
                    {
                        e.Graphics.FillEllipse(Brushes.DarkGoldenrod, x, y, 42, 42);
                        e.Graphics.FillEllipse(Brushes.GreenYellow, x + 5, y + 5, 32, 32);
                    }

                    if (current_board.map[i, j].isGoal)
                    {
                        e.Graphics.FillRectangle(Brushes.Red, x, y, 43, 43);
                        e.Graphics.DrawImage(Image.FromFile("star.png"), x + 5, y + 5, 34, 32);
                    }

                    if (current_board.map[i, j].hewan != null)
                    {
                        //nanti ganti image disini
                        Brush brush;
                        if (current_board.map[i, j].hewan.belongsTo == "Player 1")
                        {
                            brush = new SolidBrush(Color.Black);
                        }
                        else
                        {
                            brush = new SolidBrush(Color.Brown);
                        }
                        //e.Graphics.DrawString(current_board.map[i, j].hewan.name, new Font("Courier New", 12), brush, x + 5, y + 12);
                        e.Graphics.DrawImage(current_board.map[i, j].hewan.img,x,y,43,43);
                        //
                    }

                    //hint
                    if (clickedloc[0] != new Point(-1, -1))
                    {
                        if (clickedloc[0].X == j && clickedloc[0].Y == i) //belum check tipe binatang
                        {
                            Animal animal = current_board.map[i, j].hewan;
                            if (i > 0) //up
                            {
                                if (current_board.map[i - 1, j].isLand || (animal.name == "Mou" && giliran == "Player 1"))
                                {
                                    up = true;
                                }
                            }

                            if (i < 8) //down
                            {
                                if (current_board.map[i + 1, j].isLand || (animal.name == "Mou" && giliran == "Player 2"))
                                {
                                    down = true;
                                }
                            }

                            if (j > 0) //left
                            {
                                left = true;
                            }

                            if (j < 6) //right
                            {
                                right = true;
                            }

                        }
                    }
                }
            }

            if (up)
            {
                e.Graphics.FillEllipse(Brushes.White, (clickedloc[0].X + 1) * 44 + 16, (clickedloc[0].Y) * 44 + 16, 10, 10);
            }

            if (down)
            {
                e.Graphics.FillEllipse(Brushes.White, (clickedloc[0].X + 1) * 44 + 16, (clickedloc[0].Y + 2) * 44 + 16, 10, 10);
            }

            if (left)
            {
                e.Graphics.FillEllipse(Brushes.White, (clickedloc[0].X) * 44 + 16, (clickedloc[0].Y + 1) * 44 + 16, 10, 10);
            }

            if (right)
            {
                e.Graphics.FillEllipse(Brushes.White, (clickedloc[0].X + 2) * 44 + 16, (clickedloc[0].Y + 1) * 44 + 16, 10, 10);
            }
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    Rectangle rectangle = new Rectangle((j + 1) * 44, (i + 1) * 44, 44, 44);
                    if (counter == -1 && rectangle.Contains(e.Location) && current_board.map[i, j].hewan != null && current_board.map[i, j].hewan.belongsTo == giliran)
                    {
                        counter += 1;
                        clickedloc[counter] = new Point((rectangle.Location.X / 44) - 1, (rectangle.Location.Y / 44) - 1);
                        break;
                    }
                    else if (counter == 0 && rectangle.Contains(e.Location))
                    {
                        counter += 1;
                        clickedloc[counter] = new Point((rectangle.Location.X / 44) - 1, (rectangle.Location.Y / 44) - 1);
                        break;
                    }
                }
            }

            if (counter == 1)
            {
                //sudah klik button 2x, tentukan action
                if (movePawn(clickedloc[0].X, clickedloc[0].Y, clickedloc[1].X, clickedloc[1].Y, giliran))
                {
                    //move yang dilakukan valid, ganti giliran
                    if (giliran == "Player 1")
                    {
                        giliran = "Player 2";
                        //AIMove();
                    }
                    else
                    {
                        giliran = "Player 1";
                    }
                }
                else
                {
                    //move yang dilakukan tidak valid, masih giliran yg sama
                }
                label1.Text = giliran + "'s Turn";
                counter = -1;
                clickedloc = new Point[2];
                clickedloc[0] = new Point(-1, -1);
                clickedloc[1] = new Point(-1, -1);
            }
            Invalidate();
        }


        //Function KEVIN
        public bool movePawn(int xawal, int yawal, int xtuju, int ytuju, string movingPlayer)
        {
            //pergerakan antar petak
            if (current_board.map[yawal, xawal].hewan != null && current_board.map[yawal, xawal].hewan.belongsTo == movingPlayer)
            {
                if (current_board.map[ytuju, xtuju].hewan != null)
                {
                    //sama sama di darat, baru boleh saling memakan
                    if(current_board.map[yawal, xawal].isLand && current_board.map[ytuju, xtuju].isLand)
                    {
                        //makan pion lain
                        if (current_board.map[yawal, xawal].hewan.grade == 1 && plusmove(xawal, yawal, xtuju, ytuju))
                        {
                            if (current_board.map[ytuju, xtuju].hewan.grade == 8 && current_board.map[ytuju, xtuju].hewan.belongsTo != movingPlayer)
                            {
                                //tikus makan gajah
                                current_board.map[ytuju, xtuju].hewan = current_board.map[yawal, xawal].hewan;
                                current_board.map[yawal, xawal].hewan = null;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else if (current_board.map[yawal, xawal].hewan.grade == 7 || current_board.map[yawal, xawal].hewan.grade == 6)
                        {
                            MessageBox.Show("Makan");
                            //----PERGERAKAN TIGER & LION-------
                            //lion dan tiger dapat memakan dengan melompati air
                            //hitung jarak lompatan
                            int jaraky = ytuju - yawal;
                            int jarakx = xtuju - xawal;
                            if (Math.Abs(jarakx) > 1)
                            {
                                //jika jarak lbh dr 1, hanya valid ketika melewati air
                                bool allwater = true;
                                if (jarakx < -1)
                                {
                                    //jarak x lbh kecil dr -1, berarti geser ke kiri
                                    for (int i = -1; i > jarakx; i--)
                                    {
                                        if (current_board.map[yawal, xawal + i].isLand || current_board.map[yawal, xawal + i].hewan != null)
                                        {
                                            //jika ada 1 saja yg bersifat land, maka tdk valid
                                            allwater = false;
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 1; i < jarakx; i++)
                                    {
                                        //jarak x lbh besar dr 1, berarti geser ke kanan
                                        if (current_board.map[yawal, xawal + i].isLand || current_board.map[yawal, xawal + i].hewan != null)
                                        {
                                            //jika ada 1 saja yg bersifat land, maka tdk valid
                                            allwater = false;
                                        }
                                    }
                                }
                                if (!allwater)
                                {
                                    //ada yg tidak air, tidak valid move nya
                                    return false;
                                }
                            }
                            else if (Math.Abs(jaraky) > 1)
                            {
                                //sama dengan jarakx, hanya ini membandingkan y
                                bool allwater = true;
                                if (jaraky < -1)
                                {
                                    for (int i = -1; i > jaraky; i--)
                                    {
                                        if (current_board.map[yawal + i, xawal].isLand || current_board.map[yawal + i, xawal].hewan != null)
                                        {
                                            allwater = false;
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 1; i < jaraky; i++)
                                    {
                                        if (current_board.map[yawal + i, xawal].isLand || current_board.map[yawal + i, xawal].hewan != null)
                                        {
                                            allwater = false;
                                        }
                                    }
                                }
                                if (!allwater)
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                //jika jarak x maupun y tidak lbh dr 1, maka hanya bisa move 4 arah mata angin saja
                                if (!plusmove(xawal, yawal, xtuju, ytuju))
                                {
                                    return false;
                                }
                            }
                            if (current_board.map[ytuju, xtuju].hewan.grade < current_board.map[yawal, xawal].hewan.grade && current_board.map[ytuju, xtuju].hewan.belongsTo != movingPlayer)
                            {
                                //cek apakah pion msuh lebih lemah dr pion mu, jika ya, dimakan
                                current_board.map[ytuju, xtuju].hewan = current_board.map[yawal, xawal].hewan;
                                current_board.map[yawal, xawal].hewan = null;
                            }
                            else
                            {
                                //pion msuh lbh kuat, tdk valid
                                return false;
                            }
                        }
                        else if (current_board.map[yawal, xawal].hewan.grade == 8 && plusmove(xawal, yawal, xtuju, ytuju))
                        {
                            if (current_board.map[ytuju, xtuju].hewan.grade != 1 && current_board.map[ytuju, xtuju].hewan.belongsTo != movingPlayer)
                            {
                                //pionmu lebih kuat, makan pion lawan
                                current_board.map[ytuju, xtuju].hewan = current_board.map[yawal, xawal].hewan;
                                current_board.map[yawal, xawal].hewan = null;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            if (current_board.map[yawal, xawal].hewan.grade > current_board.map[ytuju, xtuju].hewan.grade && current_board.map[ytuju, xtuju].hewan.belongsTo != movingPlayer && plusmove(xawal, yawal, xtuju, ytuju))
                            {
                                //pionmu lebih kuat, makan pion lawan
                                current_board.map[ytuju, xtuju].hewan = current_board.map[yawal, xawal].hewan;
                                current_board.map[yawal, xawal].hewan = null;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    //pindah tile saja
                    if (current_board.map[ytuju, xtuju].isGoal && plusmove(xawal, yawal, xtuju, ytuju))
                    {
                        //masuk goal
                        if(movingPlayer == "Player 2" && ytuju == 0)
                        {
                            return false;
                        }
                        else if (movingPlayer == "Player 2" && ytuju == 8)
                        {
                            current_board.map[ytuju, xtuju].hewan = current_board.map[yawal, xawal].hewan;
                            current_board.map[yawal, xawal].hewan = null;
                            //menang
                            MessageBox.Show("Player 2 Win!");
                        }

                        if (movingPlayer == "Player 1" && ytuju == 8)
                        {
                            return false;
                        }
                        else if (movingPlayer == "Player 1" && ytuju == 0)
                        {
                            current_board.map[ytuju, xtuju].hewan = current_board.map[yawal, xawal].hewan;
                            current_board.map[yawal, xawal].hewan = null;
                            //menang
                            MessageBox.Show("Player 1 Win!");
                        }
                    }
                    else if (current_board.map[ytuju, xtuju].isTrap && plusmove(xawal, yawal, xtuju, ytuju))
                    {
                        //masuk trap
                        current_board.map[ytuju, xtuju].hewan = current_board.map[yawal, xawal].hewan;
                        current_board.map[ytuju, xtuju].hewan.grade = 0;
                        current_board.map[yawal, xawal].hewan = null;
                    }
                    else
                    {
                        if (!current_board.map[ytuju, xtuju].isLand && plusmove(xawal, yawal, xtuju, ytuju))
                        {
                            if (current_board.map[yawal, xawal].hewan.grade == 1)
                            {
                                //tikus-boleh ke air
                                current_board.map[ytuju, xtuju].hewan = current_board.map[yawal, xawal].hewan;
                                current_board.map[yawal, xawal].hewan = null;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            //sama dengan PERGERAKAN TIGER & LION diatas, hanya saja tidak membamdingkan kekuatan pion, dikarenakan tidak memakan pion lain, hanya perpindah tempat
                            if (current_board.map[yawal, xawal].hewan.grade == 7 || current_board.map[yawal, xawal].hewan.grade == 6)
                            {
                                int jaraky = ytuju - yawal;
                                int jarakx = xtuju - xawal;
                                if (Math.Abs(jarakx) > 1)
                                {
                                    bool allwater = true;
                                    if (jarakx < -1)
                                    {
                                        for (int i = -1; i > jarakx; i--)
                                        {
                                            if (current_board.map[yawal, xawal + i].isLand || current_board.map[yawal, xawal + i].hewan != null)
                                            {
                                                allwater = false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 1; i < jarakx; i++)
                                        {
                                            if (current_board.map[yawal, xawal + i].isLand || current_board.map[yawal, xawal + i].hewan != null)
                                            {
                                                allwater = false;
                                            }
                                        }
                                    }
                                    if (!allwater)
                                    {
                                        return false;
                                    }
                                }
                                else if (Math.Abs(jaraky) > 1)
                                {
                                    bool allwater = true;
                                    if (jaraky < -1)
                                    {
                                        for (int i = -1; i > jaraky; i--)
                                        {
                                            if (current_board.map[yawal + i, xawal].isLand || current_board.map[yawal + i, xawal].hewan != null)
                                            {
                                                allwater = false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 1; i < jaraky; i++)
                                        {
                                            if (current_board.map[yawal + i, xawal].isLand || current_board.map[yawal + i, xawal].hewan != null)
                                            {
                                                allwater = false;
                                            }
                                        }
                                    }
                                    if (!allwater)
                                    {
                                        return false;
                                    }
                                }
                                else
                                {
                                    if (!plusmove(xawal, yawal, xtuju, ytuju))
                                    {
                                        return false;
                                    }
                                }
                                current_board.map[ytuju, xtuju].hewan = current_board.map[yawal, xawal].hewan;
                                current_board.map[yawal, xawal].hewan = null;
                            }
                            else
                            {
                                if (plusmove(xawal, yawal, xtuju, ytuju))
                                {
                                    current_board.map[ytuju, xtuju].hewan = current_board.map[yawal, xawal].hewan;
                                    current_board.map[yawal, xawal].hewan = null;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
                return true;
            }
            //gagal move pawn, giliran masih di player sebelumnya
            return false;
        }

        private bool plusmove(int xawal, int yawal, int xtuju, int ytuju)
        {
            if ((xtuju == xawal - 1 && ytuju == yawal) || (xtuju == xawal && ytuju == yawal - 1) || (xtuju == xawal + 1 && ytuju == yawal) || (xtuju == xawal && ytuju == yawal + 1))
            {
                return true;
            }
            return false;
        }
    }
}
