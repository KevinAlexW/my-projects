import pygame
from pygame.locals import *
from pygame.constants import *

from OpenGL.GL import *
from OpenGL.GLU import *

import random
import math
from objloader import *

ground_vertices = (
    (-25,-0.1,50),
    (25,-0.1,50),
    (-25,-0.1,-300),
    (25,-0.1,-300),
    )

surfaces = (
    (0,1,2,3),
    (3,2,7,6),
    (6,7,5,4),
    (4,5,1,0),
    (1,5,7,2),
    (4,0,3,6)
    )

target = [[2,-1,-2], [2,-3,-2] , [1, -2,-2], [3, -2, -2]]

edges2 = [[0, 1], [2, 3]]
cubes = []
weapons = []

weaponactive = 0
level = 1
medal = 0
health = 5
max_health = 5
gameover = False
getrifle = False
status = ""
jumlahmusuh = 0
index = -1

def nextlevel():
    for i in range(0, len(cubes)):
        cubes[i].x = random.randrange(-15, 10)
        cubes[i].y = -1
        cubes[i].z = random.randrange(-20, -10)  
        cubes[i].hp = 4 + level
    cubes.append(OBJ("BlueEyesUltimate.obj", swapyz=True))
    cubes[i].x = random.randrange(-15, 10)
    cubes[i].y = -1
    cubes[i].z = random.randrange(-20, -10)
    
def Ground():
    glBegin(GL_QUADS)
    x = 0
    for vertex in ground_vertices:
        x+=1
        glColor3fv((0,1,1))
        glVertex3fv(vertex)
    glEnd()
    glColor3f(1,1,1)

def cursor():
    glBegin(GL_LINES)
    glColor3f(1, 0, 0)
    for i in range(4):
        glVertex3f(target[i][0], target[i][1], target[i][2])
    glEnd()
    glColor3f(1,1,1)
    
def drawText(x, y, z, size, r, g, b, textString):     
    font = pygame.font.Font (None, size)
    textSurface = font.render(textString, True, (r,g,b,255), (0,0,0,255))     
    textData = pygame.image.tostring(textSurface, "RGBA", True)     
    glRasterPos3d(GLdouble ( x ) , GLdouble ( y ) , GLdouble ( z ))     
    glDrawPixels(textSurface.get_width(), textSurface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textData)


pygame.init()
display = (800,600)
pygame.display.set_mode(display, DOUBLEBUF|OPENGL)
glLoadIdentity()
gluPerspective(45.0, 800/600, 1, 100)
glTranslatef(-2, -1, -20)

clock = pygame.time.Clock()
ground = OBJ("SnowTerrainRotated.obj", swapyz=True)
ground.x = 10
ground.y = -3
ground.z = 3
weapons.append(OBJ("LaserPistol.obj", swapyz=True))
weapons[0].x = 6.5
weapons[0].y = -3
weapons[0].z = 5
weapons[0].damage = 1
weapons.append(OBJ("LaserRifle.obj", swapyz=True))
weapons[1].x = 6.5
weapons[1].y = -3
weapons[1].z = 5
weapons[1].damage = 3
jumlahmusuh = 1#random.randrange(2, 5)
for i in range(jumlahmusuh):
    cubes.append(OBJ("BlueEyesUltimate.obj", swapyz=True))
    cubes[i].x = random.randrange(-15, 10)
    cubes[i].y = -1
    cubes[i].z = random.randrange(-20, -10)   


def main():
    global jumlahmusuh, level, status, gameover, index, weaponactive, getrifle, medal, health, max_health

    while True:
        pygame.mouse.set_visible(False)
        if not gameover:
            mx, my = pygame.mouse.get_pos()
            for i in range(4):
                if i == 0:
                    target[i][0] = mx/40.5 - 10 + 2
                    target[i][1] = (600 - my)/100.5
                elif i == 1:
                    target[i][0] = mx/40.5 - 10 + 2
                    target[i][1] = (600 - my)/100.5 - 4
                elif i == 2:
                    target[i][0] = mx/40.5 - 12 + 2
                    target[i][1] = (600 - my)/100.5 - 2
                elif i == 3:
                    target[i][0] = mx/40.5 - 8 + 2
                    target[i][1] = (600 - my)/100.5 - 2
        for i in range(0, len(cubes)):
            if cubes[i].y < 100:
                if cubes[i].x < (weapons[weaponactive].x - 5):
                    cubes[i].x += 0.1
                elif cubes[i].x > (weapons[weaponactive].x - 5):
                    cubes[i].x -= 0.1
                cubes[i].z += 0.1
                if cubes[i].z >= weapons[weaponactive].z + 2:
                    if health > 0:
                        cubes[i].y = 999
                        jumlahmusuh -= 1
                        index = -1
                        health -= 1
                        if jumlahmusuh <= 0:
                            level += 1
                            medal += 1
                            status = "pause"
                            gameover = True
                    else :
                        gameover = True
                        status = "kalah"
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if status == "pause":
                    if gameover:
                        if event.key == pygame.K_1 and medal >= 5:
                            getrifle = True
                            medal -= 5
                        elif event.key == pygame.K_2 and medal >= 1:
                            weapons[0].damage += 0.25
                            weapons[1].damage += 0.75
                            medal -= 1
                        elif event.key == pygame.K_3 and medal >= 1:
                            health = max_health
                            medal -= 1
                        elif event.key == pygame.K_4 and medal >= 1:
                            max_health += 1
                            health += 1
                            medal -= 1
                    gameover = False
                    status = ""
                    nextlevel()
                    jumlahmusuh = len(cubes)
                    print(jumlahmusuh)
                if event.key == pygame.K_1:
                    weaponactive = 0
                if event.key == pygame.K_2:
                    if getrifle:
                        weaponactive = 1
                if event.key == pygame.K_w:
                    for i in range(0, len(cubes)):
                        cubes[i].z += 1
                        ground.z += 0.1
                if event.key == pygame.K_a:
                    for i in range(0, len(cubes)):
                        cubes[i].x += 1
                        ground.x += 0.1
                if event.key == pygame.K_s:
                    for i in range(0, len(cubes)):
                        cubes[i].z -= 1
                        ground.z -= 0.1
                if event.key == pygame.K_d:
                    for i in range(0, len(cubes)):
                        cubes[i].x -= 1
                        ground.x -= 0.1
                if event.key == pygame.K_LEFT:
                    for i in range(0, len(cubes)):
                        cubes[i].rotatey += 15
                if event.key == pygame.K_RIGHT:
                    for i in range(0, len(cubes)):
                        cubes[i].rotatey -= 15
                if event.key == pygame.K_UP:
                    for i in range(0, len(cubes)):
                        cubes[i].rotatex += 15
                if event.key == pygame.K_DOWN:
                    for i in range(0, len(cubes)):
                        cubes[i].rotatey -= 15
            if event.type == pygame.MOUSEBUTTONDOWN and not gameover:
                if event.button == 1:
                    for i in range(0, len(cubes)):
                        if cubes[i].x < target[3][0] and cubes[i].x > target[2][0] and cubes[i].y < target[0][1] and cubes[i].y > target[1][1] and cubes[i].y < 100:
                            index = i
                            if cubes[i].hp - weapons[weaponactive].damage <= 0 and cubes[i].y < 100:
                                cubes[i].y = 999
                                jumlahmusuh -= 1
                                index = -1
                            elif cubes[i].hp - weapons[weaponactive].damage > 0 and cubes[i].y < 100:
                                cubes[i].hp -= weapons[weaponactive].damage
                                if cubes[i].hp < 0:
                                    cubes[i].hp = 0
                            if jumlahmusuh <= 0:
                                level += 1
                                medal += 1
                                status = "pause"
                                gameover = True

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        glPushMatrix()
        glTranslate(ground.x, ground.y, ground.z)
        glCallList(ground.gl_list)
        glPopMatrix()
        if not gameover:
            for i in range(0, len(cubes)):
                if cubes[i].y < 100:
                    glPushMatrix()
                    glTranslate(cubes[i].x, cubes[i].y, cubes[i].z)
                    glCallList(cubes[i].gl_list)
                    glPopMatrix()
                
            cursor()
            glPushMatrix()
            glTranslate(weapons[weaponactive].x, weapons[weaponactive].y, weapons[weaponactive].z)
            glCallList(weapons[weaponactive].gl_list)
            glPopMatrix()
            if index > -1:
                drawText(-8, 8, 1, 16, 255, 0, 0, "HP: " + str(cubes[index].hp))
        else:
            if status == "kalah":
                drawText(-1.5, 1, 1, 64, 255, 0, 0, "High Score : " + str(level))
            else :
                drawText(-2, 3, 1, 32, 0, 255, 0, "Select an Upgrade :")
                drawText(-1, 2, 1, 16, 255, 255, 0, "1. Unlock new Weapon (5 Medal)")
                drawText(-1, 1.7, 1, 16, 255, 255, 255, "2. Upgrade Weapon Power (1 Medal)")
                drawText(-1, 1.4, 1, 16, 255, 255, 255, "3. Heal HP (1 Medal)")
                drawText(-1, 1.1, 1, 16, 255, 255, 255, "4. Increase HP (1 Medal)")
                drawText(-1, 0.8, 1, 16, 255, 255, 255, "Press any key to exit store")

        drawText(10, 8, 1, 32, 255, 255, 255, "Level : " + str(level))
        drawText(9, 7, 1, 16, 255, 255, 255, "Health : " + str(health) + "/" + str(max_health))
        drawText(-8, -6, 1, 16, 255, 255, 255, "Medal : " + str(medal))
        pygame.display.flip()
        pygame.time.wait(20)
        
main()
